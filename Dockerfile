#  Create a new image from the base nodejs 8 image.
FROM node:8

# Create the target directory in the image
RUN mkdir -p /usr/src/tecnicas-tp0

# Set the created directory as the working directory
WORKDIR /usr/src/tecnicas-tp0

# Copy the package.json inside the working directory
COPY package.json /usr/src/tecnicas-tp0

# Install required dependencies
RUN npm install

# Copy the application source files.
COPY . /usr/src/tecnicas-tp0

# Install nodemon
RUN npm install nodemon

# Open 3000 port. This is the port that our development server uses
EXPOSE 3000

# Start the application. This is the same as running ng serve.
#CMD ["npm", "start"]