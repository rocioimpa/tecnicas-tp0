var expect = require("chai").expect;
var should = require('should');
var assert = require('assert');

var Maze = require('../src/model/maze').model.Maze;
var Cell = require('../src/model/cell').model.Cell;

describe("Maze", function () {

    describe('Validations', function () {

        it('should be an object', function () {
            let structure = {"cells": [[0,0],
                                       [0,1]],
                             "impassableTypes": []}

            let maze = new Maze(structure);

            assert(maze!=undefined);
            assert.equal(1,1);
        });

        it('cell should be an object', function(){
            let cell = new Cell(1,2,0,[3,4]);
            assert(cell!=undefined);
        });

        it('cell should have the values passed to the object', function(){
            let cell = new Cell(1,2,0,[3,4]);
            assert(cell.x == 1);
            assert(cell.y == 2);
            assert(cell.value == 0);
        });

        it('cells with different coordinates should be not equal', function(){
            let cell1 = new Cell(1,2,0,[3,4]);
            let cell2 = new Cell(3,5,0,[3,4]);

            cell1.equals(cell2).should.be.eql(false);
        });

        it('should return that the cell is non walkable', function(){
            let cell = new Cell(1,2,3,[3,4]);

            cell.isWalkable.should.be.eql(false);
        });       

        it('should have a property a with value 10.....', function () {
            let obj = {a:10};
            obj.should.have.property("a").eql(10);
            obj.a.should.be.eql(10);
        });



        it('should be a valid maze', function () {
            let structure = {"cells": [[0,0,0,0,0,0,0],
                                       [0,1,1,1,1,1,0],
                                       [0,1,2,1,2,1,0],
                                       [0,1,1,1,1,1,0],
                                       [0,0,0,0,0,0,0]],
                             "impassableTypes": [0,2]}

            let maze = new Maze(structure);

            let result = maze.validate();
            assert.equal(result,"Valid maze");
        });

        it('should be an invalid maze because there is a walkable border', function () {
            let structure = {"cells": [[0,0,0,0,0,0,0],
                                       [0,1,1,1,1,1,0],
                                       [0,1,2,1,2,1,0],
                                       [0,1,1,1,1,1,0],
                                       [0,0,0,1,0,0,0]],
                             "impassableTypes": [0,2]}

            let maze = new Maze(structure);

            let result = maze.validate();
            result.should.containEql("There is at least one walkable cell in the borders")
        });

        it('should be an invalid maze because it has only one walkable cell', function () {
            let structure = {"cells": [[0,0,0,0,0,0,0],
                                       [0,0,0,0,0,0,0],
                                       [0,0,2,0,2,0,0],
                                       [0,1,0,0,0,0,0],
                                       [0,0,0,0,0,0,0]],
                             "impassableTypes": [0,2]}

            let maze = new Maze(structure);

            let result = maze.validate();
            result.should.containEql("The maze has less than two walkable cells")
        });

        it('should be an invalid maze because it is disconnected', function () {
            let structure = {"cells": [[0,0,0,0,0,0,0],
                                       [0,1,0,1,0,1,0],
                                       [0,1,2,1,2,1,0],
                                       [0,1,0,0,0,1,0],
                                       [0,0,0,0,0,0,0]],
                             "impassableTypes": [0,2]}

            let maze = new Maze(structure);

            let result = maze.validate();
            result.should.containEql("There are less than two connections in a walkable cell")
        });

        it('should be an invalid maze because a cell cannot be reached from itself in less than four moves', function () {
            let structure = {"cells": [[0,0,0,0,0,0,0],
                                       [0,1,1,0,0,1,0],
                                       [0,1,1,0,2,1,0],
                                       [0,1,0,0,0,1,0],
                                       [0,0,0,0,0,0,0]],
                             "impassableTypes": [0,2]}

            let maze = new Maze(structure);

            let result = maze.validate();
            result.should.containEql("Can't return to cell in less than four moves")
        });

        it('should be an invalid maze because there is no path between two walkable cells', function () {
            let structure = {"cells": [[0,0,0,0,0,0,0,0,0],
                                       [0,1,1,1,0,1,1,1,0],
                                       [0,1,0,1,0,1,0,1,0],
                                       [0,1,1,1,0,1,1,1,0],
                                       [0,0,0,0,0,0,0,0,0]],
                            "impassableTypes": [0,2]}

            let maze = new Maze(structure);

            let result = maze.validate();
            result.should.containEql("Missing path between two walkable cells")
        });
    });
});


