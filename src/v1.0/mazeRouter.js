let express = require('express');
let Maze = require('../model/maze').model.Maze;
let mazeRouter = express.Router();

mazeRouter.route('/').post((request, response) => {
    let structure = request.body;
    let maze = new Maze(structure);
    let result = maze.validate();

    if(result == "Valid maze") response.status('200').send(result);
    else response.status('422').send(result);
  });

mazeRouter.route('/').get(
    (request, response) => response.send('Trabajo Practico 0 - Validador de Laberintos')
)

mazeRouter.use((req, res) => {
  console.log(res.body)
  res.status(422).send('');
})

module.exports = mazeRouter;