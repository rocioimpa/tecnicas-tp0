let express = require('express');
let bodyParser = require('body-parser');
let path = require('path');
let mazeResponse = require('../v1.0/mazeRouter');

const port = 3000;

    createServer();

function createServer() {
    var app = express();

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    app.set('port', port);

    app.use(express.static(path.join(__dirname, 'static')));

    var router = express.Router();
    router.use('/maze', mazeResponse);
    app.use('/v1.0', router);

    var server = app.listen(app.get('port'), () => {
      console.log("Express server listening on port %d ", port);
    });

}