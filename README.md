# tecnicas-tp0

##### FIUBA - Tecnicas de Diseño (75.10) 
##### Trabajo Practico 0  - Primer Cuatrimestre 2019

_Autor_: Rocio Impaglione
_Padron_: 94178

#### Modo de uso:
Posicionarse sobre el directorio donde se encuentra el trabajo, abrir una terminal de comandos y escribir 
`$ docker-compose up`

En caso de retornar el siguiente error "ERROR: Couldn't connect to Docker daemon at http+docker://localhost - is it running?
If it's at a non-standard location, specify the URL with the DOCKER_HOST environment variable.", correrlo como administrador 
`$ sudo docker-compose up`

Docker procedera a instalar todas las dependencias necesarias. Una vez finalizado, podra verse el siguiente mensaje:
`$ node_js_1  | Express server listening on port 3000 `

La aplicacion estara lista para usarse. Solo basta abrir un navegador y dirigirse a http://localhost:3000/v1.0/maze

#### Hipotesis y Supuestos:
- Se asume que las filas de un laberinto dado deben ser todas del mismo tamanio